<?php

namespace RateFoodBackendBundle\Controller;

use RateFoodBackendBundle\Form\RestaurantType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RestaurantController extends Controller
{
    public function IndexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant');
        $restaurants = $repository->findAll();

        return $this->render('RateFoodBackendBundle:Restaurant:index.html.twig',
            compact('restaurants')
        );
    }

    public function EditAction($id, Request $request)
    {
//        $request = $this->get('request');
//        echo $request->getLocale();
//        exit;
        $translated = $this->get('translator')->trans('Symfony is great');

        return new Response($translated);

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant');
        $restaurant = $repository->find($id);

        $form = $this->createForm(RestaurantType::class, $restaurant);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
            return $this->redirectToRoute('rate_food_backend_restaurant_edit', ['id' => $form->getData()->getId()]);
        }

        return $this->render('RateFoodBackendBundle:Restaurant:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
