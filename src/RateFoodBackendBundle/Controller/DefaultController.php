<?php

namespace RateFoodBackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RateFoodBackendBundle:Default:index.html.twig');
    }
}
