<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'test');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('page=1', $crawler->text());

        $crawler = $client->request('GET', 'test?page=e12');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('page=e12', $crawler->text());
    }
}
