<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RestaurantController extends Controller
{
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant');
        $restaurants = $repository->findAll();

        return $this->render('restaurants/index.html.twig',
            compact('restaurants')
        );
    }

    public function detailsAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant');
        $restaurant = $repository->find($id);

        return $this->render('restaurants/details.html.twig',
            compact('restaurant')
        );
    }

}
