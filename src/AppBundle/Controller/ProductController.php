<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductController extends Controller
{
    public function detailsAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Product');

        $product = $repository->find($id);

        return $this->render('products/details.html.twig',
            compact('product')
        );
    }

}
